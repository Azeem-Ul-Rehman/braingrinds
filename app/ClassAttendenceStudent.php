<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassAttendenceStudent extends Model
{
    protected $table = "class_attendence_students";
    protected $guarded = [];


    public function classAttendence()
    {
        return $this->belongsTo(ClassAttendence::class, 'class_attendence_id', 'id');
    }

    public function student()
    {
       return $this->belongsTo(User::class, 'student_id', 'id');
    }


}
