<?php

namespace App\Http\Resources;

use App\School;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ChildrenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (!empty($this->pic_path)) {
            if (is_null($this->gender)) {
                $profilePic = 'https://img.icons8.com/color/48/000000/guest-male--v1.png';
            } else {
                if (strtolower($this->gender) == trans('male')) {
                    $profilePic = $this->pic_path;
                } else {
                    $profilePic = $this->pic_path;
                }
            }

        } else {
            if (is_null($this->gender)) {
                $profilePic = 'https://img.icons8.com/color/48/000000/guest-male--v1.png';
            } else {
                if (strtolower($this->gender) == trans('male')) {
                    $profilePic = 'https://img.icons8.com/color/48/000000/guest-male--v1.png';
                } else {
                    $profilePic = 'https://img.icons8.com/color/48/000000/businesswoman.png';
                }
            }

        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'role' => $this->role,
            'email' => $this->email,
            'address' => $this->address,
            'about' => $this->about,
            'pic_path' => $profilePic,
            'phone_number' => $this->phone_number,
            'school_code' => $this->code,
            'school' => new SchoolResource(School::where('code', $this->code)->first()),
            'student_code' => $this->student_code,
            'birthday' => $this->studentInfo ? $this->studentInfo->birthday : '',
            'section' => new SectionResource($this->section),
            'father_name' => $this->studentInfo ? $this->studentInfo->father_name : '',
            'father_national_id' => $this->studentInfo ? $this->studentInfo->father_national_id : '',
            'father_phone_number' => $this->studentInfo ? $this->studentInfo->father_phone_number : '',
            'mother_name' => $this->studentInfo ? $this->studentInfo->mother_name : '',
            'mother_phone_number' => $this->studentInfo ? $this->studentInfo->mother_phone_number : '',
            'mother_national_id' => $this->studentInfo ? $this->studentInfo->mother_national_id : '',
            'user_type' => $this->role,
        ];
    }
}
