<?php

namespace App\Http\Resources;

use App\Course;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'section_number' => $this->section_number,
            'room_number' => $this->room_number,
            'class' => new ClassResource($this->class),
            'course' => new CourseResource(Course::where('section_id',$this->id)->first()),
        ];
    }
}
