<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeaveApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'subject'                   => $this->subject,
            'description'               => $this->description,
            'start_date'                => $this->start_date,
            "start_day"                 => date('d', strtotime($this->start_date)),
            "start_month"               => date('M', strtotime($this->start_date)),
            "start_year"                => date('Y', strtotime($this->start_date)),
            'end_date'                  => $this->end_date,
            "end_day"                   => date('d', strtotime($this->end_date)),
            "end_month"                 => date('M', strtotime($this->end_date)),
            "end_year"                  => date('Y', strtotime($this->end_date)),
            'type'                      => $this->type,
            'status'                    => $this->status,
            'attachment'                => !is_null($this->attachment) ? $this->attachment_path : false

        ];
    }
}
