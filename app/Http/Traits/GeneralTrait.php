<?php

namespace App\Http\Traits;


trait GeneralTrait
{
    public function loginResponse($status, $token, $data, $status_code, $message, $error)
    {
        $response = [
            'status' => $status,
            'token' => $token,
            'data' => $data,
            'status_code' => $status_code,
            'message' => $message,
            'error' => $error
        ];

        return $response;
    }

    public function response($status, $token, $data, $status_code, $message, $error)
    {
        $response = [
            'status' => $status,
            'data' => $data,
            'status_code' => $status_code,
            'message' => $message,
            'error' => $error
        ];

        return $response;
    }
}
