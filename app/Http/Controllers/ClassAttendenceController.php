<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClassAttendanceResource;
use App\Http\Resources\CourseResource;
use App\Http\Resources\SectionResource;
use App\Http\Resources\UserResource;
use App\User;
use App\Course;
use App\Myclass;
use App\Section;
use App\ClassAttendence;
use Illuminate\Http\Request;
use App\ClassAttendenceStudent;
use Illuminate\Support\Facades\Auth;
use App\Services\Course\CourseService;
use App\Services\Attendance\AttendanceService;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class ClassAttendenceController extends Controller
{
    protected $attendanceService;
    protected $courseService;

    public function __construct(AttendanceService $attendanceService, CourseService $courseService)
    {
        $this->attendanceService = $attendanceService;
        $this->courseService = $courseService;
    }

    public function index($teacher_id, $course_id, $exam_id, $section_id, $class_id, $date)
    {
        $class = Myclass::where('id', $class_id)->first();
        $section = Section::where('id', $section_id)->first();
        $teacher = User::where('id', $teacher_id)->first();
        $course = Course::where('id', $course_id)->first();
        // View attendances of students of a section
        $students = $this->attendanceService->getStudentsBySection($section_id);
        $attendences = ClassAttendence::where(['class_id' => $class_id, 'section_id' => $section_id, 'teacher_id' => Auth::id(), 'course_id' => $course_id, 'attendence_date' => $date])->with('classAttendenceStudent')->first();

        return view('course-class-attendance.attendance', compact('attendences', 'students', 'class', 'section', 'teacher', 'course', 'teacher_id', 'course_id', 'exam_id', 'section_id', 'class_id'));
    }

    public function store(Request $request)
    {
        if (!empty($request->students) && count($request->students)) {
            if (isset($request->attendence_id) && !is_null($request->attendence_id)) {
                $classAttendence = ClassAttendence::find($request->attendence_id);
                $message = 'Updated';
            } else {
                $classAttendence = new ClassAttendence();
                $message = 'Saved';
            }
            $classAttendence->section_id = $request->section_id;
            $classAttendence->class_id = $request->class_id;
            $classAttendence->course_id = $request->course_id;
            $classAttendence->teacher_id = $request->teacher_id;
            $classAttendence->attendence_date = date('Y-m-d');
            $classAttendence->save();
            if (isset($request->attendence_id) && !is_null($request->attendence_id)) {
                $classAttendence->classAttendenceStudent()->delete();
            }
            foreach ($request->students as $key => $student) {
                $studentAttendence = new ClassAttendenceStudent();
                $studentAttendence->class_attendence_id = $classAttendence->id;
                $studentAttendence->student_id = $student;
                $presentStatus = $request->input('isPresent' . $key . '');
                $absentStatus = $request->input('isAbsent' . $key . '');
                $leaveStatus = $request->input('isLeave' . $key . '');

                if (isset($presentStatus) && !is_null($presentStatus)) {
                    $studentAttendence->status = 'present';
                }
                if (isset($absentStatus) && !is_null($absentStatus)) {
                    $studentAttendence->status = 'absent';
                }
                if (isset($leaveStatus) && !is_null($leaveStatus)) {
                    $studentAttendence->status = 'leave';
                }
                $studentAttendence->save();
            }
        }

        return redirect()->route('course.teacher.index', [$request->teacher_id, 0])->with('status', __($message));
    }

    public function yearlyClassAttendence($student_id, $course_id, $exam_id, $section_id, $class_id)
    {
        try {
            $user = User::where('id', $student_id)->first();
            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $studentCourse = CourseResource::collection(Course::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'id' => $course_id])->get());
            $yearSession = [];
            $students = $this->attendanceService->getStudentsBySection($section_id);
            $course = Course::where('id', $course_id)->first();
            if (!empty($studentCourse) && count($studentCourse) > 0) {
                foreach ($studentCourse as $attendence) {
                    $currentSessionYear = ClassAttendence::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') new_date"), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                        ->groupby('year', 'month')
                        ->pluck('new_date')->toArray();
                    foreach ($currentSessionYear as $session) {
                        $yearSession[$session] = [
                            'Present' => ClassAttendenceStudent::where('status', 'present')
                                ->where('student_id', $student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Absent' => ClassAttendenceStudent::where('status', 'absent')
                                ->where('student_id', $student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Leave' => ClassAttendenceStudent::where('status', 'leave')
                                ->where('student_id', $student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                        ];
                    }

                }
            }
            return view('course-class-attendance.yearly', compact('yearSession', 'students', 'course', 'student_id', 'section_id', 'class_id', 'exam_id', 'course_id'));

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function dateFilter(Request $request)
    {
        if (!is_null($request->date)) {
            return redirect()->route('course.class.attendence', [$request->teacher_id, $request->course_id, $request->exam_id, $request->section_id, $request->class_id, date('Y-m-d', strtotime($request->date))]);

        } else {
            return redirect()->back()->with('status', __('Please Select Date'));
        }

    }

    public function listOfStudentCoursesOfParticularLoginUser($student_id, $teacher_id)
    {
        $teacherCourses = Course::where('teacher_id', $teacher_id)->pluck('section_id')->toArray();
        $students = User::whereIn('section_id', $teacherCourses)->where('id', $student_id)->with('section', 'section.course', 'section.class')->get();
        return view('course-class-attendance.student-attendence-course', compact('students'));

    }

    public function monthCourseWiseAttendence($student_id, $course_id, $exam_id, $section_id, $class_id, $date)
    {
        try {

            $user = User::where('id', $student_id)->first();
            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $studentCourse = CourseResource::collection(Course::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'id' => $course_id])->get());
            $students = $this->attendanceService->getStudentsBySection($section_id);
            $course = Course::where('id', $course_id)->first();
            if (!empty($studentCourse) && count($studentCourse) > 0) {
                foreach ($studentCourse as $attendence) {
                    $monthAttendence = ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $date) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->whereMonth('attendence_date', '=', date('m', strtotime($date)));
                    })->where('student_id', $student_id)->get();


                }
            }
            return view('course-class-attendance.month', compact('monthAttendence', 'students', 'course', 'student_id', 'section_id', 'class_id', 'exam_id', 'course_id'));

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }
}
