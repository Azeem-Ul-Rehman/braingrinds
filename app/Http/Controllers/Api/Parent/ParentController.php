<?php

namespace App\Http\Controllers\Api\Parent;

use App\Http\Resources\ChildrenResource;
use App\Http\Resources\LeaveApplicationResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\GeneralTrait;
use App\StudentInfo;
use App\User;
use App\Course;
use App\Section;
use Carbon\Carbon;
use App\ClassAttendence;
use App\LeaveApplication;
use App\AssignActivityItems;
use Illuminate\Http\Request;
use App\ClassAttendenceStudent;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Traits\AttendenceTrait;
use App\Http\Resources\CourseResource;
use App\Http\Resources\SectionResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ClassAttendanceResource;
use Illuminate\Validation\Rule;

class ParentController extends Controller
{
    use AttendenceTrait;
    use GeneralTrait;

    public $successStatus = 200;

    // Class Activity
    public function getStudentActivity(Request $request)
    {
        try {
            $user = User::where('id', $request->student_id)->first();

            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $acitvityClass = AssignActivityItems::whereHas('assignActivity', function ($q) use ($request, $sectionWithClass) {
                $q->where('section_id', $sectionWithClass->id)->where('class_id', $sectionWithClass->class->id)->where('date', Carbon::today())->where('type', 'class');
            })->get();
            $acitvityHome = AssignActivityItems::whereHas('assignActivity', function ($q) use ($request, $sectionWithClass) {
                $q->where('section_id', $sectionWithClass->id)->where('class_id', $sectionWithClass->class->id)->where('date', Carbon::today())->where('type', 'home');
            })->get();

            $success['home-activity'] = $acitvityHome;
            $success['class-activity'] = $acitvityClass;
            $success['section-class'] = $sectionWithClass;


            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Student Activity', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function getStudentActivityWithDate(Request $request)
    {
        try {
            $user = User::where('id', $request->student_id)->first();

            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());

            $date = null == $request->date ? null : \DateTime::createFromFormat('Y-m-d', $request->date);
            $acitvityClass = AssignActivityItems::whereHas('assignActivity', function ($q) use ($request, $date, $sectionWithClass) {
                $q->where('section_id', $sectionWithClass->id)
                    ->where('class_id', $sectionWithClass->class->id)
                    ->where('date', $date->format('Y-m-d'))
                    ->where('type', 'class');
            })->get();
            $acitvityHome = AssignActivityItems::whereHas('assignActivity', function ($q) use ($request, $date, $sectionWithClass) {
                $q->where('section_id', $sectionWithClass->id)
                    ->where('class_id', $sectionWithClass->class->id)
                    ->where('date', $date->format('Y-m-d'))
                    ->where('type', 'home');
            })->get();

            $success['home-activity'] = $acitvityHome;
            $success['class-activity'] = $acitvityClass;
            $success['section-class'] = $sectionWithClass;

            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Student Activity with date', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    // Class Attendence

    public function getStudentCurrentMonthAttendence(Request $request)
    {
        try {
            $workingDays = $this->countDays(date('Y'), date('m'), [0, 6]);
            $user = User::where('id', $request->student_id)->first();

            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $studentCourse = CourseResource::collection(Course::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id])->get());
            $currentMonthAttendence = [];
            $yearSession = [];

            if (!empty($studentCourse) && count($studentCourse) > 0) {
                foreach ($studentCourse as $attendence) {
                    $present = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->whereMonth('attendence_date', '=', date('m'));
                    })->where('status', 'present')->whereMonth('created_at', '=', date('m'))->where('student_id', $request->student_id)->get());

                    $absent = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->whereMonth('attendence_date', '=', date('m'));
                    })->where('status', 'absent')->whereMonth('created_at', '=', date('m'))->where('student_id', $request->student_id)->get());

                    $leave = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->whereMonth('attendence_date', '=', date('m'));
                    })->where('status', 'leave')->whereMonth('created_at', '=', date('m'))->where('student_id', $request->student_id)->get());

                    $currentSessionYear = ClassAttendence::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') new_date"), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                        ->groupby('year', 'month')
                        ->pluck('new_date')->toArray();

                    foreach ($currentSessionYear as $session) {
                        $yearSession[$session] = [
                            'Present' => ClassAttendenceStudent::where('status', 'present')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Absent' => ClassAttendenceStudent::where('status', 'absent')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Leave' => ClassAttendenceStudent::where('status', 'leave')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                        ];
                    }

                    $currentMonthAttendence[$attendence->course_name] = [
                        'Present' => $present,
                        'Absent' => $absent,
                        'Leave' => $leave,
                        'CurrentSession' => $yearSession,
                    ];
                }
            }

            $success['working-days'] = $workingDays;
            $success['courses'] = $studentCourse;
            $success['currentMonthAttendence'] = $currentMonthAttendence;

            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Student Current Month Attendence', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function getStudentCurrentMonthAttendenceWithDate(Request $request)
    {
        try {
            $workingDays = $this->countDays(date('Y'), date('m', strtotime($request->get('date'))), [0, 6]);
            $user = User::where('id', $request->student_id)->first();

            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $studentCourse = CourseResource::collection(Course::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id])->get());
            $currentMonthAttendence = [];
            $yearSession = [];

            if (!empty($studentCourse) && count($studentCourse) > 0) {
                foreach ($studentCourse as $attendence) {
                    $present = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $request) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])
                            ->whereMonth('attendence_date', '=', date('m', strtotime($request->get('date'))));
                    })->where('status', 'present')->whereMonth('created_at', '=', date('m', strtotime($request->get('date'))))->where('student_id', $request->student_id)->get());

                    $absent = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $request) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])
                            ->whereMonth('attendence_date', '=', date('m', strtotime($request->get('date'))));
                    })->where('status', 'absent')->whereMonth('created_at', '=', date('m', strtotime($request->get('date'))))->where('student_id', $request->student_id)->get());

                    $leave = ClassAttendanceResource::collection(ClassAttendenceStudent::whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $request) {
                        $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])
                            ->whereMonth('attendence_date', '=', date('m', strtotime($request->get('date'))));
                    })->where('status', 'leave')->whereMonth('created_at', '=', date('m', strtotime($request->get('date'))))->where('student_id', $request->student_id)->get());

                    $currentSessionYear = ClassAttendence::where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id])
                        ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') new_date"), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
                        ->groupby('year', 'month')
                        ->pluck('new_date')->toArray();

                    foreach ($currentSessionYear as $session) {
                        $yearSession[$session] = [
                            'Present' => ClassAttendenceStudent::where('status', 'present')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Absent' => ClassAttendenceStudent::where('status', 'absent')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                            'Leave' => ClassAttendenceStudent::where('status', 'leave')
                                ->where('student_id', $request->student_id)
                                ->whereMonth('created_at', '=', date('m', strtotime($session)))
                                ->whereHas('classAttendence', function ($q) use ($attendence, $sectionWithClass, $session) {
                                    $q->where(['class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id, 'teacher_id' => $attendence->teacher->id, 'course_id' => $attendence->id]);
                                })->count(),
                        ];
                    }

                    $currentMonthAttendence[$attendence->course_name] = [
                        'Present' => $present,
                        'Absent' => $absent,
                        'Leave' => $leave,
                        'CurrentSession' => $yearSession,
                    ];
                }
            }

            $success['working-days'] = $workingDays;
            $success['courses'] = $studentCourse;
            $success['currentMonthAttendence'] = $currentMonthAttendence;

            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Student Current Month Attendence with date', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    // Leave Application

    public function requestLeaveApplication(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'type' => 'required',
                'subject' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'student_id' => 'required',
                //  'course_id' => 'required',
//                'class_id' => 'required'  ,
//                'teacher_id' => 'required',
            ], [
                'type.required' => 'Type is required',
                'subject.required' => 'Subject is required',
                'description.required' => 'Description is required',
                'start_date.required' => 'Start Date is required',
                'end_date.required' => 'End Date is required',
                'student_id.required' => 'Student Id is required',
                //  'course_id.required' => 'Course Id is required',
//                'class_id.required' => 'Class Id is required',
//                'teacher_id.required' => 'Teacher Id is required',
            ]);

            if ($validator->fails()) {
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'Validation Error', $validator->errors()->first()), 200);
            }


            $user = User::where('id', $request->student_id)->first();

            $sectionWithClass = new SectionResource(Section::where('id', $user->section_id)->first());
            $studentCourse = Course::where([
                //  'id' => $request->course_id,
                'class_id' => $sectionWithClass->class->id, 'section_id' => $sectionWithClass->id])->with('teacher')->first();

            $leaveApplication = new LeaveApplication();
            $leaveApplication->type = $request->type;
            $leaveApplication->subject = $request->subject;
            $leaveApplication->description = $request->description;
            $leaveApplication->start_date = $request->start_date;
            $leaveApplication->end_date = $request->end_date;
            $leaveApplication->student_id = $request->student_id;
            $leaveApplication->course_id = $studentCourse->id;
            $leaveApplication->class_id = $studentCourse->class_id;
            $leaveApplication->teacher_id = $studentCourse->teacher_id;
            $leaveApplication->status = 'pending';

            if (isset($request->attachment) && !is_null($request->attachment) && $request->has('attachment')) {
                $image = $request->file('attachment');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/attachments');
                $imagePath = $destinationPath . '/' . $name;
                $image->move($destinationPath, $name);
                $attachment = $name;
                $leaveApplication->attachment = $attachment;

            }
            $leaveApplication->save();
            $success['data'] = new LeaveApplicationResource($leaveApplication);

            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Leave Application send successfully.', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function getStudentCourseLeaveApplication(Request $request)
    {

        try {

            $leaveApplication = LeaveApplicationResource::collection(LeaveApplication::where(
                [
                    'student_id' => $request->student_id,
                    // 'course_id' => $request->course_id,
                ])->orderBy('id', 'desc')->get());
            $sickLeave = LeaveApplication::where(
                [
                    'student_id' => $request->student_id,
                    //   'course_id' => $request->course_id,
                    'type' => 'sick'
                ])->orderBy('id', 'desc')->get();
            $casualLeave = LeaveApplication::where(
                [
                    'student_id' => $request->student_id,
                    //   'course_id' => $request->course_id,
                    'type' => 'casual'
                ])->orderBy('id', 'desc')->get();

            $success['data'] = $leaveApplication;
            $success['sickLeave'] = count($sickLeave);
            $success['casualLeave'] = count($casualLeave);
            return response()->json(
                $this->response(true, null, $success, $this->successStatus, 'Leave Application Record', null), $this->successStatus);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }


    // User Profile

    public function updateProfileImage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Error', $validator->errors()->first()), 200);
        }
        try {


            $uploadType = 'profile';
            $user = User::where('id', $request->student_id)->first();
            $upload_dir = 'school-' . $user->school_id . '/' . date("Y") . '/' . $uploadType;
            $path = \Storage::disk('public')->putFile($upload_dir, $request->file('image'));//$request->file('file')->store($upload_dir);

            $user->pic_path = 'storage/' . $path;
            $user->save();

            return response()->json(
                $this->response(true, null, new ChildrenResource($user), $this->successStatus, 'Profile image updated successfully.', null), 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = User::where('id', $request->student_id)->first();
        $validator = Validator::make($request->all(), [
            'mother_phone_number' => 'required',
            'father_phone_number' => 'required',
            'address' => 'required',
            'student_id' => 'required',
        ], [
            'mother_phone_number.required' => 'Mother Phone Number is required',
            'father_phone_number.required' => 'Father Phone Number is required',
            'address.required' => 'Address is required',
            'student_id.required' => 'Children Id is required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Error.', $validator->errors()->first()), 200);
        }

        try {
            $user->address = $request->address;
            $user->save();

            $studentInfo = StudentInfo::where('student_id', $request->student_id)->first();
            $studentInfo->mother_phone_number = $request->mother_phone_number;
            $studentInfo->father_phone_number = $request->father_phone_number;
            $studentInfo->save();
            return response()->json(
                $this->response(true, null, new ChildrenResource($user), $this->successStatus, 'Profile updated successfully.', null), 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
            'new-password-confirm' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Error.', $validator->errors()->first()), 200);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'Your current password does not matches with the password you provided. Please try again.', 'Your current password does not matches with the password you provided. Please try again.'), 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'New Password cannot be same as your current password. Please choose a different password.', 'New Password cannot be same as your current password. Please choose a different password.'), 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(
                    $this->response(true, null, null, $this->successStatus, 'Password Changed successfully.', null), 200);

            } else {
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'New Password must be same as your confirm password.', 'New Password must be same as your confirm password.'), 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }
}
