<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ChildrenResource;
use App\Http\Traits\GeneralTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use GeneralTrait;

    public $successStatus = 200;

    public function otp(Request $request)
    {
        $details = ['email' => $request->email];

        $validator = Validator::make($details, [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Errors', $validator->errors()->first()), $this->successStatus);
        }

        try {
            $otp_code = null;
            $user = User::where('email', $request->email)->first();

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);

            $data = [
                'email' => $user->email,
                'code' => $otp_code,
            ];
            Mail::to($user->email)->send(new VerifyMail($user, $otp_code));
            return response()->json(
                $this->response(true, null, $data, $this->successStatus, 'OTP code has been sent on given number', null), 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Errors', $validator->errors()->first()), $this->successStatus);
        }

        try {
            $user = User::where([
                'email' => $request->get('email'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {
//                $user->otp_status = 'verified';
//                $user->save();

                return response()->json(
                    $this->response(true, null, null, $this->successStatus, 'OTP Code verified successfully.', null), 200);
            } else {
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'Provided OTP code is not correct.', 'Provided OTP code is not correct.'), 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function forgotPasswordRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(
                $this->response(false, null, null, $this->successStatus, 'Validation Errors.', $validator->errors()->first()), $this->successStatus);
        }

        try {
            $user = User::where('email', $request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(
                    $this->response(false, null, null, $this->successStatus, 'Your account is not associated with this email address.', 'Your account is not associated with this email address.'), $this->successStatus);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(
                $this->response(true, null, null, $this->successStatus, 'Your password has been changes successfully.', null), 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required',
            'role' => 'required',
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',
            'role.required' => 'Role is required',
        ]);

        if ($validator->fails()) {
            return response()->json($this->response(false, null, null, $this->successStatus, 'Validation Errors', $validator->errors()->first()), $this->successStatus);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();

            if ($request->role == $user->role && $user->active == true) {
                $success['token'] = $user->createToken('AppName')->accessToken;
                $success['data'] = new UserResource($user);
            } else {
                auth()->logout();
                return response()->json($this->response(false, null, null, 401, 'Your account is not associated with this role.', 'Your account is not associated with this role'), 401);
            }
            return response()->json($this->loginResponse(true, $success['token'], $success['data'], $this->successStatus, 'Login successfully.', null), $this->successStatus);
        } else {
            return response()->json($this->response(false, null, null, 401, 'Invalid Credentials', 'Invalid Credentials'), 401);
        }
    }

    public function logout()
    {
        try {
            $user = Auth::user();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }

            return response()->json(
                $this->response(true, null, null, $this->successStatus, 'Logout successfully.', null), 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function getUser()
    {
        $user = Auth::user();
        $success['data'] = new UserResource($user);

        return response()->json(
            $this->response(true, null, $success['data'], $this->successStatus, 'User information', null), 200);
    }

    public function getChildrens()
    {
        $user = Auth::user();
        $success['data'] = ChildrenResource::collection(User::where('parent_id', $user->id)->get());

        return response()->json(
            $this->response(true, null, $success['data'], $this->successStatus, 'Childrens information', null), 200);
    }

    public function getChildrenProfile($student_id)
    {
        $success['data'] = new ChildrenResource(User::where('id', $student_id)->first());
        return response()->json(
            $this->response(true, null, $success['data'], $this->successStatus, 'Children Profile Information', null), 200);
    }
}
