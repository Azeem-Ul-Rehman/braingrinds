<?php

namespace App\Http\Controllers;

use App\LeaveApplication;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LeaveApplicationController extends Controller
{
    public function index($teacher_id, $section_id)
    {
        $leaveApplications = LeaveApplication::where('teacher_id', $teacher_id)->get();
        $view = 'leave-application.index';

        return view($view, compact('leaveApplications'));
    }

    public function updateStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

            $status = LeaveApplication::where('id', $request->get('id'))->update([
                'status' => $request->get('status'),
            ]);
            $response['status'] = 'success';

        return $response;

    }
}
