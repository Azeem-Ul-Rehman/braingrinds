<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignActivityItems extends Model
{
    protected $table = "assign_activity_items";
    protected $guarded = [];

    public function assignActivity()
    {
        return $this->belongsTo(AssignActivity::class, 'assign_activity_id', 'id');
    }
}
