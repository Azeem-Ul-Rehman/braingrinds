<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassAttendence extends Model
{
    protected $table = "class_attendences";
    protected $guarded = [];

    public function classAttendenceStudent()
    {
        return $this->hasMany(ClassAttendenceStudent::class, 'class_attendence_id', 'id');
    }

}
