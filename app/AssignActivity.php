<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignActivity extends Model
{
    protected $table = "assign_activity";
    protected $guarded = [];

    public function assignActivityItems()
    {
        return $this->hasOne(AssignActivityItems::class, 'assign_activity_id', 'id');
    }
}
