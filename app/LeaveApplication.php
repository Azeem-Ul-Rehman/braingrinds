<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    protected $table = "leave_applications";
    protected $guarded = [];


    public function student()
    {
        return $this->belongsTo(User::class, 'student_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function classs()
    {
        return $this->belongsTo(Myclass::class, 'class_id', 'id');
    }

    public function getAttachmentPathAttribute()
    {
        return asset('uploads/attachments/' . $this->attachment);
    }
}
