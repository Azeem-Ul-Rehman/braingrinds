<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassAttendenceStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_attendence_students', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('class_attendence_id');
            $table->unsignedInteger('student_id');
            $table->enum('status', ['present', 'absent', 'leave'])->default('absent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_attendence_students');
    }
}
