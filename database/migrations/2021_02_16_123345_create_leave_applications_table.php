<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('student_id');
            $table->longText('subject');
            $table->longText('description');
            $table->longText('attachment')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->enum('type', ['casual', 'sick']);
            $table->enum('status', ['pending', 'approved','rejected'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
}
