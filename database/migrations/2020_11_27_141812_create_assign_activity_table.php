<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_activity', function (Blueprint $table) {
            $table->id();
            $table->integer('teacher_id');
            $table->integer('section_id');
            $table->integer('class_id');
            $table->integer('course_id');
            $table->date('date');
            $table->enum('type', ['home', 'class'])->default('class');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_activity');
    }
}
