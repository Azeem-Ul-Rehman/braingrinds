@extends('layouts.app')

@section('title', __('Assign Activity'))

@section('content')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
    <style>
        .ck-editor__editable {
            min-height: 200px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" id="side-navbar">
                @include('layouts.leftside-menubar')
            </div>
            <div class="col-md-10" id="main-container">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="panel panel-default">
                    @if(count($students) > 0)
                        <div class="panel-body">
                            <div class="col-md-6">
                                @if(!is_null($activityCheck))
                                    <form action="{{url('course/assign/activity/store')}}" method="POST" id="msgForm">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="teacher_id" value="{{$teacher_id}}">
                                        <input type="hidden" name="section_id" value="{{$section_id}}">
                                        <input type="hidden" name="class_id" value="{{$class_id}}">
                                        <input type="hidden" name="course_id" value="{{$course_id}}">
                                        <input type="hidden" name="activity_id" value="{{$activityCheck->id}}">
                                        <input type="hidden" name="status" value="update">
                                        <input type="hidden" name="activity_item_id"
                                               value="{{$activityCheck->assignActivityItems->id}}">
                                        <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                            <label for="date">Date: </label><br>
                                            <input type="date" name="date" id="date" placeholder="dd/mm/yyyy" required
                                                   value="{{$activityCheck->date}}">
                                            @if ($errors->has('date'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="class">Class: </label><br>
                                            <input type="text" name="class" id="class" placeholder="Class"
                                                   value="{{$class->class_number}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="section">Section: </label><br>
                                            <input type="text" name="section" id="section" placeholder="Section"
                                                   value="{{$section->section_number}}" readonly>
                                        </div>
                                        <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label for="type">Activity: </label><br>
                                            <select name="type" id="type" class="form-group">
                                                <option value="">Select Activity Type</option>
                                                <option
                                                    value="home" {{$activityCheck->type == 'home' ? 'selected' : ''}}>
                                                    Home
                                                </option>
                                                <option
                                                    value="class" {{$activityCheck->type == 'class' ? 'selected' : ''}}>
                                                    Class
                                                </option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Title: </label><br>
                                            <input type="text" name="title" id="title" placeholder="Title"
                                                   value="{{$activityCheck->assignActivityItems->title}}">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div
                                            class="form-group {{ $errors->has('submission_date') ? ' has-error' : '' }}">
                                            <label for="submission_date">Submission Date: </label><br>
                                            <input type="date" name="submission_date" id="submission_date"
                                                   placeholder="dd/mm/yyyy" required
                                                   value="{{$activityCheck->assignActivityItems->submission_date}}">
                                            @if ($errors->has('submission_date'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('submission_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="msg">Description </label>
                                            <textarea name="description" class="form-control" id="msg" cols="30"
                                                      rows="10">{{$activityCheck->assignActivityItems->description}}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-danger btn-sm"><i
                                                class="material-icons">save</i> SAVE
                                        </button>
                                    </form>
                                @else
                                    <form action="{{url('course/assign/activity/store')}}" method="POST" id="msgForm">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="teacher_id" value="{{$teacher_id}}">
                                        <input type="hidden" name="section_id" value="{{$section_id}}">
                                        <input type="hidden" name="class_id" value="{{$class_id}}">
                                        <input type="hidden" name="course_id" value="{{$course_id}}">
                                        <input type="hidden" name="status" value="store">
                                        <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                            <label for="date">Date: </label><br>
                                            <input type="date" name="date" id="date" placeholder="dd/mm/yyyy" required>
                                            @if ($errors->has('date'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="class">Class: </label><br>
                                            <input type="text" name="class" id="class" placeholder="Class"
                                                   value="{{$class->class_number}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="section">Section: </label><br>
                                            <input type="text" name="section" id="section" placeholder="Section"
                                                   value="{{$section->section_number}}" readonly>
                                        </div>
                                        <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label for="type">Activity: </label><br>
                                            <select name="type" id="type" class="form-group">
                                                <option value="">Select Activity Type</option>
                                                <option value="home">Home</option>
                                                <option value="class">Class</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Title: </label><br>
                                            <input type="text" name="title" id="title" placeholder="Title">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div
                                            class="form-group {{ $errors->has('submission_date') ? ' has-error' : '' }}">
                                            <label for="submission_date">Submission Date: </label><br>
                                            <input type="date" name="submission_date" id="submission_date"
                                                   placeholder="dd/mm/yyyy" required>
                                            @if ($errors->has('submission_date'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('submission_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="msg">Description </label>
                                            <textarea name="description" class="form-control" id="msg" cols="30"
                                                      rows="10"></textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-danger btn-sm"><i
                                                class="material-icons">save</i> SAVE
                                        </button>
                                    </form>
                                @endif

                            </div>
                        </div>
                        <script>
                            $(function () {
                                var r = $(':checkbox[name="recipients[]"]');
                                $('#selectAll').on('change', function () {
                                    if ($(this).is(':checked')) {
                                        r.prop('checked', true);
                                    } else {
                                        r.prop('checked', false);
                                    }
                                });
                                ClassicEditor
                                    .create(document.querySelector('#msg'), {
                                        toolbar: ['bold', 'italic', 'Heading', 'Link', 'bulletedList', 'numberedList', 'blockQuote']
                                    })
                                    .catch(error => {
                                        console.error(error);
                                    });
                            });

                        </script>
                    @else
                        <div class="panel-body">
                            @lang('No Related Data Found.')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
