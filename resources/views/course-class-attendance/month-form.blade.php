<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th scope="col">@lang('Month')</th>
            <th scope="col">@lang('Attendence')</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($monthAttendence) && count($monthAttendence) > 0)
            @foreach($monthAttendence as $key=>$month)
                <tr>
                    <td>{{date('d M Y', strtotime($month->created_at))}}</td>
                    <td>{{ucfirst($month->status)}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                Record Not Found
            </tr>
        @endif
        </tbody>
    </table>
</div>
