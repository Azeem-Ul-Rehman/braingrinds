<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th scope="col">@lang('Course Name')</th>
            <th scope="col">@lang('Action')</th>
        </tr>
        </thead>
        <tbody>
        @if(count($students) > 0)
            @foreach ($students as $student)
                <tr>
                    <td>{{$student->section->course->course_name}}</td>
                    <td><a href="{{url('yearly/class/attendence/'.$student->id.'/'.$student->section->course->id.'/'.$student->section->id.'/'.$student->section->id.'/'.$student->section->class->id)}}">View Attendence</a></td>
                </tr>
            @endforeach
        @else
            <tr>
                Record Not Found
            </tr>
        @endif
        </tbody>
    </table>
</div>
