@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('date.course.filter') }}" method="post">
    @csrf
    <input type="hidden" name="section_id" value="{{$section_id}}">
    <input type="hidden" name="exam_id" value="{{$exam_id}}">
    <input type="hidden" name="class_id" value="{{$class_id}}">
    <input type="hidden" name="course_id" value="{{$course_id}}">
    <input type="hidden" name="teacher_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">

    <input type="date" name="date" id="date">
    <button type="submit" class="btn btn-primary">Submit</button>

</form>
<form action="{{url('course/class/attendance/take-attendance')}}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="section_id" value="{{$section_id}}">
    <input type="hidden" name="class_id" value="{{$class_id}}">
    <input type="hidden" name="course_id" value="{{$course_id}}">
    <input type="hidden" name="teacher_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('Student_Code')</th>
                <th scope="col">@lang('Name')</th>
                <th scope="col">@lang('Present')</th>
                <th scope="col">@lang('Absent')</th>
                <th scope="col">@lang('Leave')</th>
            </tr>
            </thead>
            <tbody>

            @if (!is_null($attendences))
                <input type="hidden" name="update" value="1">
                <input type="hidden" name="attendence_id" value="{{ $attendences->id }}"/>

                @foreach ($attendences->classAttendenceStudent as $attendance)
                    <input type="hidden" name="students[]" value="{{$attendance->student->id}}">
                    <tr>
                        <th scope="row">{{($loop->index + 1)}}</th>
                        <td>{{$attendance->student->student_code}}</td>
                        <td>
                            @if($attendance->status === 'present')
                                <span class="label label-success attdState">@lang('Present')</span>
                            @elseif($attendance->status === 'absent')
                                <span class="label label-danger attdState">@lang('Absent')</span>
                            @elseif($attendance->status === 'leave')
                                <span class="label label-warning attdState">@lang('Leave')</span>
                            @endif
                            <a href="{{url('yearly/class/attendence/'.$attendance->student->id.'/'.$course_id.'/'.$exam_id.'/'.$section_id.'/'.$class_id)}}">{{$attendance->student->name}}</a>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isPresent{{$loop->index}}" id="isPresent{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Present"
                                       aria-label="Present" {{ $attendance->status == 'present' ? 'checked' :'' }}>
                            </div>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isAbsent{{$loop->index}}" id="isAbsent{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Absent"
                                       aria-label="Absent" {{ $attendance->status == 'absent' ? 'checked' :'' }}>
                            </div>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isLeave{{$loop->index}}" id="isLeave{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Leave"
                                       aria-label="Leave" {{ $attendance->status == 'leave' ? 'checked' :'' }}>
                            </div>
                        </td>

                    </tr>
                @endforeach
            @else
                <input type="hidden" name="update" value="0">
                <input type="hidden" name="attendances[]" value="0">
                @foreach ($students as $student)
                    <input type="hidden" name="students[]" value="{{$student->id}}">
                    <tr>
                        <th scope="row">{{($loop->index + 1)}}</th>
                        <td>{{$student->student_code}}</td>
                        <td><span class="label label-success attdState">@lang('Present')</span>
                            <a href="{{url('yearly/class/attendence/'.$student->id.'/'.$course_id.'/'.$exam_id.'/'.$section_id.'/'.$class_id)}}">{{$student->name}}</a>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isPresent{{$loop->index}}" id="isPresent{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Present" aria-label="Present" checked>
                            </div>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isAbsent{{$loop->index}}" id="isAbsent{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Absent" aria-label="Absent">
                            </div>
                        </td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input position-static" type="checkbox"
                                       name="isLeave{{$loop->index}}" id="isLeave{{$loop->index}}"
                                       data-id="{{$loop->index}}" data-status="Leave" aria-label="Leave">
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div style="text-align:center;">
        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 2%;"
           role="button">@lang('Cancel')</a>
        @if (!is_null($attendences))
            <button type="submit" class="btn btn-primary">@lang('Update')</button>
        @else
            <button type="submit" class="btn btn-primary">@lang('Submit')</button>
        @endif
    </div>
</form>
<script>
    $('input[type="checkbox"]').change(function () {
        var attdState = $(this).parent().parent().parent().find('.attdState').removeClass('label-danger label-success');

        debugger;
        if ($(this).is(':checked')) {


            var id = $(this).data('id');
            if ($(this).data('status') == 'Present') {
                attdState.addClass('label-success').text(@json( __('Present')));
                $('#isAbsent' + id).removeAttr('checked');
                $('#isLeave' + id).removeAttr('checked');
            }
            if ($(this).data('status') == 'Absent') {
                attdState.addClass('label-danger').text(@json( __('Absent')));
                $('#isPresent' + id).removeAttr('checked');
                $('#isLeave' + id).removeAttr('checked');
            }
            if ($(this).data('status') == 'Leave') {
                attdState.addClass('label-warning').text(@json( __('Leave')));
                $('#isAbsent' + id).removeAttr('checked');
                $('#isPresent' + id).removeAttr('checked');
            }
        } else {
            attdState.addClass('label-danger').text(@json( __('Absent')));
        }
    });
</script>
