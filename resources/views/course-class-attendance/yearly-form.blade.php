<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th scope="col">@lang('Month')</th>
            <th scope="col">@lang('Present')</th>
            <th scope="col">@lang('Absent')</th>
            <th scope="col">@lang('Leave')</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($yearSession) && count($yearSession) > 0)
            @foreach($yearSession as $key=>$session)
                <tr>
                    <td><a href="{{ route('month.couse.attendence',[$student_id,$course_id,$exam_id,$section_id,$class_id,$key]) }}">{{$key}}</a></td>
                    <td>{{$session['Present']}}</td>
                    <td>{{$session['Absent']}}</td>
                    <td>{{$session['Leave']}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                Record Not Found
            </tr>
        @endif
        </tbody>
    </table>
</div>
