<div class="table-responsive">
    <table class="table table-bordered table-striped table-data-div table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('Subject')</th>
            <th scope="col">@lang('Description')</th>
            <th scope="col">@lang('Course')</th>
            <th scope="col">@lang('Class')</th>
            <th scope="col">@lang('Student')</th>
            <th scope="col">@lang('Start Date')</th>
            <th scope="col">@lang('End Date')</th>
            <th scope="col">@lang('Type')</th>
            <th scope="col">@lang('Status')</th>
            <th scope="col">@lang('Attachment')</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($leaveApplications as $appli)
            <tr>
                <th scope="row">{{($loop->index + 1)}}</th>
                <td>{{$appli->subject}}</td>
                <td>{{$appli->description}}</td>
                <td>{{!is_null($appli->course) ? $appli->course->course_name : 'Course Not Found'}}</td>
                <td>{{!is_null($appli->classs) ? $appli->classs->class_number : 'Class Not Found'}}</td>
                <td>{{!is_null($appli->student) ? $appli->student->name : 'Student Not Found'}}</td>
                <td>{{$appli->start_date}}</td>
                <td>{{$appli->end_date}}</td>
                <td>{{$appli->type}}</td>
                <td>
                    <select id="status"
                            class="form-control attendence-status"
                            name="status" autocomplete="status"
                            data-application-id="{{$appli->id}}">
                        <option value="">Select an option</option>
                        <option value="pending" {{ (old('status',$appli->status) == 'pending') ? 'selected' : ''  }}>
                            Pending
                        </option>
                        <option value="approved" {{ (old('status',$appli->status) == 'approved') ? 'selected' : ''  }}>
                            Approved
                        </option>
                        <option value="rejected" {{ (old('status',$appli->status) == 'rejected') ? 'selected' : ''  }}>
                            Rejected
                        </option>
                    </select>

                </td>
                <td>
                    @if(!is_null($appli->attachment))
                        <img src="{{$appli->attachment_path}}" alt="{{$appli->attachment}}" style="width: 25%;" onclick="imageModal('{{$appli->attachment_path}}')">
                    @else
                        Img Not Found
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@push('modals')

    <!-- Modal -->
    <div class="modal fade" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="imgModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Attachment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="" alt="" id="imageAttachment" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
@endpush
@push('jquery')
    <script>
        function imageModal(img) {
            $('#imageAttachment').attr('src', img);
            $('#imgModal').modal('show');
        }
    </script>
    <script>
        $(document).on('change', '.attendence-status', function () {

            form = $(this).closest('form');
            node = $(this);
            var status = $(this).val();
            var id = $(this).data('application-id');
            var request = {"status": status, "id": id};
            if (status !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.update.leave.application.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr['success']("Leave Application " + status + " Successfully.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Status");
            }

        });
    </script>
@endpush
