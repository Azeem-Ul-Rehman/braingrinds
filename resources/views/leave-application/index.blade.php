@extends('layouts.app')

@section('title', __('Leave Application'))

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" id="side-navbar">
                @include('layouts.leftside-menubar')
            </div>
            <div class="col-md-10" id="main-container">
                <h2>@lang('Leave Applications')</h2>
                <div class="panel panel-default">
                    @if(count($leaveApplications) > 0)
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @component('leave-application.leave-app-table',['leaveApplications'=>$leaveApplications])
                            @endcomponent
                        </div>
                    @else
                        <div class="panel-body">
                            @lang('No Related Data Found.')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
